# 使用方式
## 第一步
把src/main/resources/sql文件夹下的sql文件导入到数据库中，
# 第二步
修改com.zhz.mutil.application.service.DataSourceBizService.init()方法，修改数据源的配置，这个数据源是test数据库的数据源，只要作用是保存数据库的动态配置
![img.png](src/main/resources/doc/img.png)
# 第三步
启动项目，访问不同的api，具体如下


# API说明

# 动态数据源

## POST 数据源分页查询

POST /localhost:8080/operDb/find

> 返回示例

> 200 Response

```json
{
  "code": 0,
  "msg": "string",
  "data": [
    {
      "url": null,
      "username": null,
      "password": null,
      "driverClassName": null,
      "type": null,
      "poolName": "string"
    }
  ],
  "ok": true
}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

状态码 **200**

| 名称               | 类型     | 必选  | 约束 | 中文名                                                | 说明 |
| ------------------ | -------- | ----- | ---- | ----------------------------------------------------- | ---- |
| » code             | integer  | true  | none |                                                       | none |
| » msg              | string   | true  | none |                                                       | none |
| » data             | [object] | true  | none |                                                       | none |
| »» url             | null     | false | none | 数据库URL                                             | none |
| »» username        | null     | false | none | 数据库用户名                                          | none |
| »» password        | null     | false | none | 数据库密码                                            | none |
| »» driverClassName | null     | false | none | 数据库驱动                                            | none |
| »» type            | null     | false | none | 数据库类型，不区分大小写（myqsl，oracle，postgresql） | none |
| »» poolName        | string   | false | none | 唯一标识，标明当前数据源的唯一标识                    | none |
| » ok               | boolean  | true  | none |                                                       | none |

## POST 添加数据源

POST /localhost:8080/operDb/add

> Body 请求参数

```json
{
  "url": "string",
  "username": "string",
  "password": "string",
  "type": "string",
  "poolName": "string"
}
```

### 请求参数

| 名称       | 位置 | 类型   | 必选 | 中文名                             | 说明 |
| ---------- | ---- | ------ | ---- | ---------------------------------- | ---- |
| body       | body | object | 否   |                                    | none |
| » url      | body | string | 是   | 数据库URL                          | none |
| » username | body | string | 是   | 数据库用户名                       | none |
| » password | body | string | 是   | 数据库密码                         | none |
| » type     | body | string | 是   | 数据库类型                         | none |
| » poolName | body | string | 是   | 唯一标识，标明当前数据源的唯一标识 | none |

> 返回示例

> 200 Response

```json
{
  "code": 0,
  "msg": "string",
  "data": "string",
  "ok": true
}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

状态码 **200**

| 名称   | 类型    | 必选 | 约束 | 中文名 | 说明 |
| ------ | ------- | ---- | ---- | ------ | ---- |
| » code | integer | true | none |        | none |
| » msg  | string  | true | none |        | none |
| » data | string  | true | none |        | none |
| » ok   | boolean | true | none |        | none |

## POST 修改数据源

POST /localhost:8080/operDb/edit

> Body 请求参数

```json
{
  "url": "string",
  "username": "string",
  "password": "string",
  "type": "string",
  "poolName": "string"
}
```

### 请求参数

| 名称       | 位置 | 类型   | 必选 | 中文名                             | 说明 |
| ---------- | ---- | ------ | ---- | ---------------------------------- | ---- |
| body       | body | object | 否   |                                    | none |
| » url      | body | string | 是   | 数据库URL                          | none |
| » username | body | string | 是   | 数据库用户名                       | none |
| » password | body | string | 是   | 数据库密码                         | none |
| » type     | body | string | 是   | 数据库类型                         | none |
| » poolName | body | string | 是   | 唯一标识，标明当前数据源的唯一标识 | none |

> 返回示例

> 200 Response

```json
{
  "code": 0,
  "msg": "string",
  "data": "string",
  "ok": true
}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

状态码 **200**

| 名称   | 类型    | 必选 | 约束 | 中文名 | 说明 |
| ------ | ------- | ---- | ---- | ------ | ---- |
| » code | integer | true | none |        | none |
| » msg  | string  | true | none |        | none |
| » data | string  | true | none |        | none |
| » ok   | boolean | true | none |        | none |

## POST 测试数据源

POST /localhost:8080/operDb/checked

> Body 请求参数

```json
{
  "url": "string",
  "username": "string",
  "password": "string",
  "type": "string",
  "poolName": "string"
}
```

### 请求参数

| 名称       | 位置 | 类型   | 必选 | 中文名                             | 说明 |
| ---------- | ---- | ------ | ---- | ---------------------------------- | ---- |
| body       | body | object | 否   |                                    | none |
| » url      | body | string | 是   | 数据库URL                          | none |
| » username | body | string | 是   | 数据库用户名                       | none |
| » password | body | string | 是   | 数据库密码                         | none |
| » type     | body | string | 是   | 数据库类型                         | none |
| » poolName | body | string | 是   | 唯一标识，标明当前数据源的唯一标识 | none |

> 返回示例

> 200 Response

```json
{
  "code": 0,
  "msg": "string",
  "data": "string",
  "ok": true
}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

状态码 **200**

| 名称   | 类型    | 必选 | 约束 | 中文名 | 说明 |
| ------ | ------- | ---- | ---- | ------ | ---- |
| » code | integer | true | none |        | none |
| » msg  | string  | true | none |        | none |
| » data | string  | true | none |        | none |
| » ok   | boolean | true | none |        | none |

## POST 删除数据源

POST /localhost:8080/operDb/delete

> Body 请求参数

```json
{
  "url": "string",
  "username": "string",
  "password": "string",
  "type": "string",
  "poolName": "string"
}
```

### 请求参数

| 名称       | 位置 | 类型   | 必选 | 中文名                             | 说明 |
| ---------- | ---- | ------ | ---- | ---------------------------------- | ---- |
| body       | body | object | 否   |                                    | none |
| » url      | body | string | 是   | 数据库URL                          | none |
| » username | body | string | 是   | 数据库用户名                       | none |
| » password | body | string | 是   | 数据库密码                         | none |
| » type     | body | string | 是   | 数据库类型                         | none |
| » poolName | body | string | 是   | 唯一标识，标明当前数据源的唯一标识 | none |

> 返回示例

> 200 Response

```json
{
  "code": 0,
  "msg": "string",
  "data": "string",
  "ok": true
}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

状态码 **200**

| 名称   | 类型    | 必选 | 约束 | 中文名 | 说明 |
| ------ | ------- | ---- | ---- | ------ | ---- |
| » code | integer | true | none |        | none |
| » msg  | string  | true | none |        | none |
| » data | string  | true | none |        | none |
| » ok   | boolean | true | none |        | none |

# 用户多数据源测试

## GET 001数据库测试

GET /localhost:8080/user/finalAllUser

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构

## GET 002数据库测试

GET /localhost:8080/user/finalAllUser2

> 返回示例

> 200 Response

```json
{}
```

### 返回结果

| 状态码 | 状态码含义                                              | 说明 | 数据模型 |
| ------ | ------------------------------------------------------- | ---- | -------- |
| 200    | [OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) | 成功 | Inline   |

### 返回数据结构
