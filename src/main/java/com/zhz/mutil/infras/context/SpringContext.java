package com.zhz.mutil.infras.context;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Spring上下文：显式获取SpringBean;SpringEvent事件发布
 *
 * @author zhouhengzhe
 */
@Component
public class SpringContext implements ApplicationContextAware {
    private static final Logger log = LoggerFactory.getLogger(SpringContext.class);
    private static ApplicationContext applicationContext;

    public SpringContext() {
        log.info("### BASE INIT : SpringContext ###");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        SpringContext.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext() {
        if (applicationContext == null) {
            throw new IllegalStateException("ApplicationContext未注入");
        }
        return applicationContext;
    }

    public static <T> T getBean(String name) {
        return (T) getApplicationContext().getBean(name);
    }

    public static <T> T getBean(String name, Class<T> requiredType) {
        return getApplicationContext().getBean(name, requiredType);
    }

    public static <T> T getHandler(String name, Class<T> cls) {
        T t = null;
        if (StrUtil.isNotEmpty(name)) {
            try {
                t = getApplicationContext().getBean(name, cls);
            } catch (Exception var4) {
                log.error("####################" + name + "未定义");
            }
        }

        return t;
    }

    public static <T> T getBean(Class<T> clazz) {
        return getApplicationContext().getBean(clazz);
    }


    public static <T> Collection<T> getBeans(Class<T> clazz) {
        Map<String, T> beansOfType = getApplicationContext().getBeansOfType(clazz);
        if (CollUtil.isEmpty(beansOfType)) return Collections.emptyList();
        return beansOfType.values();
    }

    public static Environment getEnv() {
        return getApplicationContext().getEnvironment();
    }

    /**
     * 定时发布事件
     */
    public static void schedule(ApplicationEvent event, Date startTime) {
        ThreadPoolTaskScheduler taskScheduler = getApplicationContext().getBean("taskScheduler", ThreadPoolTaskScheduler.class);
        taskScheduler.schedule(() -> getApplicationContext().publishEvent(Objects.requireNonNull(event)), startTime);
    }

    /**
     * 定时发布事件
     */
    public static void schedule(ApplicationEvent event, long delayMillis) {
        schedule(event, new Date(System.currentTimeMillis() + delayMillis));
    }
}