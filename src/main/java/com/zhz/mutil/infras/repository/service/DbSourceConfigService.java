package com.zhz.mutil.infras.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhz.mutil.domain.contract.dto.DbDataSourceDTO;
import com.zhz.mutil.infras.repository.entity.DbSourceConfig;

/**
 * 数据库配置(DbSourceConfig)表服务接口
 *
 * @author zhouhengzhe
 * @since 2024-10-15 16:45:06
 */
public interface DbSourceConfigService extends IService<DbSourceConfig> {

    /**
     * 新增数据源
     * @param form
     */
    void add(DbDataSourceDTO form);

    /**
     * 修改数据源
     * @param form
     */
    void modify(DbDataSourceDTO form);

    /**
     * 删除数据源
     * @param form
     */
    void delete(DbDataSourceDTO form);
}

