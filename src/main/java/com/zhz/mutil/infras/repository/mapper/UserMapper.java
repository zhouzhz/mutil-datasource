package com.zhz.mutil.infras.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhz.mutil.infras.repository.entity.User;
import org.springframework.stereotype.Repository;

/**
 * (User)表数据库访问层
 *
 * @author zhouhengzhe
 * @since 2024-10-15 15:14:46
 */
@Repository
public interface UserMapper extends BaseMapper<User> {

}

