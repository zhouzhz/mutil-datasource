package com.zhz.mutil.infras.repository.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据库配置(DbSourceConfig)表实体类
 *
 * @author zhouhengzhe
 * @since 2024-10-15 16:45:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("db_source_config")
public class DbSourceConfig {
    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;
    /**
     * url配置
     */
    private String url;
    /**
     * 用户名
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 驱动
     */

    private String driverClassName;
    /**
     * 数据库类型
     */
    private String type;
    /**
     * 数据库唯一标识（一般是公司ID）
     */
    private String poolName;

}

