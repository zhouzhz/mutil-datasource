package com.zhz.mutil.infras.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zhz.mutil.infras.repository.entity.User;

import java.util.List;

/**
 * (User)表服务接口
 *
 * @author zhouhengzhe
 * @since 2024-10-15 15:14:47
 */
public interface UserService extends IService<User> {

    List<User> selectAll();

    List<User> selectAll2() ;

}

