package com.zhz.mutil.infras.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zhz.mutil.infras.repository.entity.DbSourceConfig;

/**
 * 数据库配置(DbSourceConfig)表数据库访问层
 *
 * @author zhouhengzhe
 * @since 2024-10-15 16:45:06
 */
public interface DbSourceConfigMapper extends BaseMapper<DbSourceConfig> {

}

