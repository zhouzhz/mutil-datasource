package com.zhz.mutil.infras.repository.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zhz.mutil.infras.repository.entity.User;
import com.zhz.mutil.infras.repository.mapper.UserMapper;
import com.zhz.mutil.infras.repository.service.UserService;
import com.zhz.mutil.infras.utils.DbOprUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author zhouhengzhe
 * @since 2024-10-15 15:14:47
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    private DbOprUtil dbOprUtil;


    @Override
    public List<User> selectAll() {
        dbOprUtil.changeByDataSourceByName("001");
        return this.baseMapper.selectList(null);
    }

    @Override
    public List<User> selectAll2() {
        dbOprUtil.changeByDataSourceByName("002");
        return this.baseMapper.selectList(null);
    }
}

