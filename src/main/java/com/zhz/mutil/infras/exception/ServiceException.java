package com.zhz.mutil.infras.exception;

import com.zhz.mutil.domain.contract.enums.IEnum;
import com.zhz.mutil.domain.contract.enums.ResultCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 服务异常，可用于控制业务异常流程，抛出后由统一异常增强类捕获，返回友好提示
 *
 * @author zhouhengzhe
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ServiceException extends RuntimeException {
    private static final long serialVersionUID = 1026631908939212210L;
    protected Serializable errCode;
    protected String msg;

    public ServiceException() {
        this(ResultCodeEnum.FAIL.getCode(), ResultCodeEnum.FAIL.getDesc());
    }

    public ServiceException(String msg) {
        this(ResultCodeEnum.FAIL.getCode(), msg);
    }

    public ServiceException(String msg, Throwable e) {
        super(msg, e);
        this.errCode = ResultCodeEnum.FAIL.getCode();
        this.msg = msg;
    }

    public ServiceException(IEnum resultCode) {
        this(resultCode.getCode(), resultCode.getDesc(), null);
    }

    public ServiceException(Serializable errCode, String msg) {
        this(errCode, msg, null);
    }

    public ServiceException(Serializable errCode, String msg, Throwable cause) {
        super(msg, cause);
        this.errCode = errCode;
        this.msg = msg;
    }

    public ServiceException(Serializable code, String... params) {
        this.errCode = code;
        this.msg = String.join(",", params);
    }

    public ServiceException(Serializable code, Throwable e) {
        super(e);
        this.errCode = code;
        this.msg = e.getMessage();
    }

    public ServiceException(Serializable code, Throwable e, String... params) {
        super(e);
        this.errCode = code;
        this.msg = String.join(",", params);
    }

}
