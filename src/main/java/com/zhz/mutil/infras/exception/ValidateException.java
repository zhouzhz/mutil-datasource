package com.zhz.mutil.infras.exception;


import com.zhz.mutil.domain.contract.enums.ResultCodeEnum;
import lombok.Getter;

import java.util.Map;
import java.util.StringJoiner;

/**
 * 校验异常，可用于控制业务异常流程，抛出后由统一异常增强类捕获，返回友好提示
 *
 * @author zhouhengzhe
 */
@Getter
public class ValidateException extends ServiceException {
    private static final long serialVersionUID = 412491531709952986L;
    private Map<String, String> errorMap;

    public ValidateException(Map<String, String> errorMap) {
        super(ResultCodeEnum.PARAMETER_VALIDATION_FAILED.getCode(), ResultCodeEnum.PARAMETER_VALIDATION_FAILED.getDesc());
        this.errorMap = errorMap;
    }

    public ValidateException(String errorMsg) {
        super(ResultCodeEnum.PARAMETER_VALIDATION_FAILED.getCode(), errorMsg);
    }

    @Override
    public String toString() {
        return (new StringJoiner(", ", ValidateException.class.getSimpleName() + "[", "]")).add("code=" + this.getErrCode()).add("msg=" + this.getMessage()).add("errorMap=" + this.errorMap).toString();
    }
}
