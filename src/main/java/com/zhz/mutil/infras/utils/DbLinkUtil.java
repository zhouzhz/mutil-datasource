package com.zhz.mutil.infras.utils;


import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 校验数据源连接是否成功
 *
 * @author zhouhengzhe
 */
@Slf4j
@UtilityClass
public class DbLinkUtil {

    /**
     * 校验数据源连接是否成功,不成功抛异常
     */
    public boolean verifyUrlConnStatus(String url, String driverClassName, String username, String password) {

        boolean flag;
        Connection connection = null;
        // 加载驱动类
        try {
            Class.forName(driverClassName);
            connection = DriverManager.getConnection(url, username, password);
            flag = true;
        } catch (Exception e) {
            log.error("数据库链接错误", e);
            flag = false;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                log.error("SQL error！", e);
                flag = false;
            }
        }
        return flag;
    }
}