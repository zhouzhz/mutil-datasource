package com.zhz.mutil;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 启动类
 *
 * @author zhouhengzhe
 * @date 2024/10/15
 */
@SpringBootApplication
@EnableScheduling
@MapperScan("com.zhz.mutil.infras.repository.mapper")
public class DataSourceApplication  {

    public static void main(String[] args) {
        SpringApplication.run(DataSourceApplication.class, args);
    }
}
