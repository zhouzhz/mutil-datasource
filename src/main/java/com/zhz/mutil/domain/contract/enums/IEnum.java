package com.zhz.mutil.domain.contract.enums;

import java.io.Serializable;

/**
 * 双值枚举接口
 *
 * @author zhouhengzhe
 */
public interface IEnum<T extends Serializable> {
    /**
     * 获取枚举的code
     *
     * @return
     */
    T getCode();

    /**
     * 获取枚举的描述信息
     *
     * @return
     */
    String getDesc();
}
