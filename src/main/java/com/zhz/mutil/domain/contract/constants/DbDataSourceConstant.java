package com.zhz.mutil.domain.contract.constants;

/**
 * 数据库数据源常量类
 *
 * @author zhouhengzhe
 */
public class DbDataSourceConstant {

    /**
     * oracle 驱动名称
     */
    public static final String ORACLE_DRIVER_CLASS_NAME = "oracle.jdbc.driver.OracleDriver";

    /**
     * mysql 低版本驱动名称(6.0以下)
     */
    public static final String MYSQL_LOW_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

    /**
     * mysql 高版本驱动名称(6.0以上)
     */
    public static final String MYSQL_HIGH_DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";
    /**
     * postgresql 驱动名称
     */
    public static final String POSTGRESQL_HIGH_DRIVER_CLASS_NAME = "org.postgresql.Driver";
}