package com.zhz.mutil.domain.contract.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author zhouhengzhe
 * @date 2024/10/15
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DbDataSourceDTO {
    private String url;
    private String username;
    private String password;
    private String driverClassName;
    private String type;
    private String poolName;
}
