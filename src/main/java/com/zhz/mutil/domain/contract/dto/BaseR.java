package com.zhz.mutil.domain.contract.dto;

import java.io.Serializable;

/**
 * 统一接口响应，标准的响应数据结构
 *
 * @author zhouhengzhe
 */
public interface BaseR extends Serializable {
    /**
     * 获取code
     *
     * @return code
     */
    Serializable getCode();

    /**
     * 获取msg
     *
     * @return msg
     */
    String getMsg();

    /**
     * 获取返回的数据
     *
     * @param <T> 数据具体类型
     * @return data
     */
    <T> T getData();

    /**
     * 是否返回成功
     *
     * @return true/false
     */
    Boolean isOk();

}