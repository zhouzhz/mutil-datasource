package com.zhz.mutil.domain.contract.constants;

/**
 * 常用上下文对象常量名，本地线程持有
 *
 * @author zhouhengzhe
 */
public class ContextConstants {
    public static final String REQUEST_URL = "request-url";
    public static final String REQUEST_PARAMS = "request-params";
    public static final String TENANT_ID = "tenant-id";
    public static final String SYSTEM_ID = "system-id";
    public static final String SHOP_ID = "shop-id";
    public static final String THIRD_SESSION = "third-session";
    public static final String SESSION_CONTEXT = "session-context";
    public static final String CLIENT_TYPE = "client-type";
    public static final String APP_ID = "app-id";
    public static final String IP = "ip";
    public static final String USER_ID = "user-id";
    public static final String ACCESS_TOKEN = "access-token";
    public static final String AUTHORIZATION = "Authorization";
    public static final String SEE = "see";

    /**
     * 对应权限系统的系统管理中畅指云控制平台的ID值
     */
    public static final Integer CURRENT_SYSTEM_ID=6;
    public static final String ADMIN_TENANT_ID="-1";
    public static final String ADMIN_ROLE ="控制台平台管理员";

}
