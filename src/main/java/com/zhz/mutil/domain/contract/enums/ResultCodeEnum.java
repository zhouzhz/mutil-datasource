package com.zhz.mutil.domain.contract.enums;

import lombok.AllArgsConstructor;

/**
 * 常用错误码定义
 *
 * @author zhouhengzhe
 */
@AllArgsConstructor
public enum ResultCodeEnum implements IEnum<Integer> {
    OK(200, "请求/操作成功"),
    FAIL(500, "请求成功但是服务异常"),
    UNAUTHORIZED(401, "未登录或token已经失效"),
    FORBIDDEN(403, "没有权限"),
    NO_ACCESS_TOKEN(403001, "没有权限，缺少AccessToken"),
    SERVER_ERROR(500, "服务器异常"),
    REQUEST_ERROR(400, "请求异常"),

    ILLEGAL_ARGUMENT_EX(406,"非法状态/参数异常"),
    MISSING_SERVLET_REQUEST_PARAMETER(407,"遗失request请求参数"),
    NULL_POINT(408,"空指针异常，请查看是否没判空"),
    MEDIA_TYPE_EX(409, "请求类型异常"),
    PARAMETER_VALIDATION_FAILED(410, "参数校验失败"),
    REQUIRED_FILE_PARAM_EX(411, "请求中必须至少包含一个有效文件"),
    BASE_VALID_PARAM(412, "统一验证参数异常"),
    SQL_EX(413, "运行SQL出现异常"),
    METHOD_NOT_ALLOWED(414, "不支持当前请求类型"),
    UPLOAD_FILE_ERROR(415, "上传文件异常"),
    TIMEOUT(60001, "登录超时，请重新登录"),
    NO_SESSION(60002, "session不能为空"),
    LOGIN_FIRST(60003, "请先登录"),
    NOT_FUND_APP_KEY(60004,"路由key不正确")
    ;

    private Integer code;
    private String desc;

    /**
     * 获取枚举的code
     *
     * @return
     */
    @Override
    public Integer getCode() {
        return this.code;
    }

    /**
     * 获取枚举的描述信息
     *
     * @return
     */
    @Override
    public String getDesc() {
        return this.desc;
    }
}