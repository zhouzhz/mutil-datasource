package com.zhz.mutil.api.client;


import com.zhz.mutil.domain.contract.dto.R;
import com.zhz.mutil.infras.repository.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * (User)表控制层
 *
 * @author zhouhengzhe
 * @since 2024-10-15 16:13:06
 */
@RestController
@RequestMapping("user")
public class UserController  {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 查询所有数据
     *
     * @return 所有数据
     */
    @GetMapping("finalAllUser")
    public R selectAll() {
        return R.ok(this.userService.selectAll());
    }

    /**
     * 查询所有数据
     *
     * @return 所有数据
     */
    @GetMapping("finalAllUser2")
    public R selectAll2() {
        return R.ok(this.userService.selectAll2());
    }

}

