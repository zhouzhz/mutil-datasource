package com.zhz.mutil.api.client;

import com.zhz.mutil.application.service.DataSourceBizService;
import com.zhz.mutil.domain.contract.dto.DbDataSourceDTO;
import com.zhz.mutil.domain.contract.dto.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * 数据源控制器
 *
 * @author zhouhengzhe
 */
@RestController
@RequestMapping("/operDb")
public class DataSourceController {

    @Autowired
    private DataSourceBizService dataSourceBizService;

    /**
     * 数据源分页查询
     */
    @PostMapping(value = "find")
    public R find() {
        List<DbDataSourceDTO> result = dataSourceBizService.find();
        return R.ok(result);
    }

    /**
     * 添加数据源
     * 以下面数据为例：
     * poolName:my_demo_02
     * type:mysql
     * driverClassName:com.mysql.cj.jdbc.Driver
     * url:jdbc:mysql://127.0.0.1:3306/my_demo_02?characterEncoding=utf8&useSSL=false&autoReconnect=true&allowPublicKeyRetrieval=true&serverTimezone=GMT%2B8
     * username:root
     * password:root
     */
    @PostMapping(value = "/add")
    public R add(@RequestBody @Valid DbDataSourceDTO form) {
        dataSourceBizService.add(form);
        return R.ok();
    }

    /**
     * 测试数据源
     */
    @PostMapping(value = "/checked")
    public R checked(@RequestBody @Valid DbDataSourceDTO form) {
        dataSourceBizService.checked(form);
        return R.ok();
    }

    /**
     * 修改数据源
     */
    @PostMapping(value = "/edit")
    public R edit(@RequestBody @Valid DbDataSourceDTO form) {
        dataSourceBizService.update(form);
        return R.ok();
    }

    /**
     * 删除数据源
     */
    @PostMapping(value = "/delete")
    public R delete(@RequestBody DbDataSourceDTO form) {
        dataSourceBizService.delete(form);
        return R.ok();
    }
}