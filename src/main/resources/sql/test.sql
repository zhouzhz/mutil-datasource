/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 80027
 Source Host           : 127.0.0.1:3308
 Source Schema         : test

 Target Server Type    : MySQL
 Target Server Version : 80027
 File Encoding         : 65001

 Date: 15/10/2024 18:02:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for db_source_config
-- ----------------------------
DROP TABLE IF EXISTS `db_source_config`;
CREATE TABLE `db_source_config` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `url` varchar(512) NOT NULL COMMENT 'url配置',
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `driver_class_name` varchar(100) NOT NULL COMMENT '驱动',
  `type` varchar(20) DEFAULT NULL COMMENT '数据库类型',
  `pool_name` varchar(20) DEFAULT NULL COMMENT '数据库唯一标识（一般是公司ID）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='数据库配置';

-- ----------------------------
-- Records of db_source_config
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
